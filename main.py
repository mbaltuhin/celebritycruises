import os

import requests
from datetime import datetime as date

import xlsxwriter


def write_file_to_excell(array):
    userhome = os.path.expanduser('~')
    now = date.now()
    path_to_file = userhome + '\\Dropbox\\XLSX\\' + str(now.year) + '-' + str(now.month) + '-' + str(
        now.day) + '/' + str(now.year) + '-' + str(now.month) + '-' + str(now.day) + '-Celebrity Cruises.xlsx'
    if not os.path.exists(userhome + '\\Dropbox\\XLSX\\' + str(now.year) + '-' + str(
            now.month) + '-' + str(now.day)):
        os.makedirs(
            userhome + '\\Dropbox\\XLSX\\' + str(now.year) + '-' + str(now.month) + '-' + str(now.day))
    workbook = xlsxwriter.Workbook(path_to_file)

    worksheet = workbook.add_worksheet()
    worksheet.set_column("A:A", 15)
    worksheet.set_column("B:B", 25)
    worksheet.set_column("C:C", 10)
    worksheet.set_column("D:D", 25)
    worksheet.set_column("E:E", 20)
    worksheet.set_column("F:F", 30)
    worksheet.set_column("G:G", 20)
    worksheet.set_column("H:H", 50)
    worksheet.set_column("I:I", 20)
    worksheet.set_column("J:J", 20)
    worksheet.set_column("K:K", 20)
    worksheet.set_column("L:L", 20)
    worksheet.set_column("M:M", 25)
    worksheet.set_column("N:N", 20)
    worksheet.set_column("O:O", 20)
    worksheet.set_column("P:P", 21)
    worksheet.write('A1', 'DestinationCode')
    worksheet.write('B1', 'DestinationName')
    worksheet.write('C1', 'VesselID')
    worksheet.write('D1', 'VesselName')
    worksheet.write('E1', 'CruiseID')
    worksheet.write('F1', 'CruiseLineName')
    worksheet.write('G1', 'ItineraryID')
    worksheet.write('H1', 'BrochureName')
    worksheet.write('I1', 'NumberOfNights')
    worksheet.write('J1', 'SailDate')
    worksheet.write('K1', 'ReturnDate')
    worksheet.write('L1', 'InteriorBucketPrice')
    worksheet.write('M1', 'OceanViewBucketPrice')
    worksheet.write('N1', 'BalconyBucketPrice')
    worksheet.write('O1', 'SuiteBucketPrice')
    worksheet.write('P1', 'PortList')
    col = 0
    row = 1
    for result in array:
        for item in result:
            if col == {11, 12, 13, 14}:
                try:
                    worksheet.write_number(row, col, item)
                except ValueError:
                    worksheet.write_string(row, col, str(item))
            elif col == {9, 10}:
                worksheet.write_datetime(row, col, item)
            else:
                worksheet.write_string(row, col, str(item))
            col += 1
        row += 1
        col = 0
    workbook.close()


headers = {
    "x-currency-code": "USD",
    "x-country-code": "USA",
    "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36",
    "accept": "application/json, text/plain, */*",
    "accept-encoding": "gzip, deflate, br",
    "referer": "https://www.celebritycruises.com/itinerary-search"
}
token = requests.post("https://www.celebritycruises.com/prd/token", headers=headers).json()["token"]
authorized_headers = {
    "x-currency-code": "USD",
    "x-country-code": "USA",
    "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36",
    "accept": "application/json, text/plain, */*",
    "accept-encoding": "gzip, deflate, br",
    "referer": "https://www.celebritycruises.com/itinerary-search",
    "authorization": "token " + token["jwt"]
}
results = []
parsed_results = []
response = requests.get(
    "https://www.celebritycruises.com/prd/cruises/?&office=MIA&bookingType=FIT&cruiseType=CO&includeResults=true&count=10&offset=0&groupBy=PACKAGE&sortBy=PRICE&sortOrder=ASCENDING",
    headers=authorized_headers).json()
for index in range(0, response["hits"], 100):
    response = requests.get(
        "https://www.celebritycruises.com/prd/cruises/?&office=MIA&bookingType=FIT&cruiseType=CO&includeResults=true&count=100&offset=" + str(
            index) + "&groupBy=PACKAGE&sortBy=PRICE&sortOrder=ASCENDING", headers=authorized_headers).json()
    for itinerary in response['packages']:
        results.append(itinerary)
for itinerary in results:
    destination_code = itinerary["destinationCode"]
    duration = itinerary["duration"]
    ship_name = itinerary["ship"]["name"]
    ship_code = itinerary["ship"]["code"]
    title = itinerary["name"]
    itinerary_id = itinerary["packageId"]
    ports = []
    for location in itinerary["itineraries"]:
        try:
            ports.append(location["port"]["name"])
        except KeyError:
            ports.append(location["location"]["name"])
    for sailing in itinerary["sailings"]:
        interior_price, outside_price, balcony_price, suite_price = "N/A", "N/A", "N/A", "N/A"
        sail_date = date.strptime(sailing["sailDate"], "%Y-%m-%d").date()
        arrive_date = date.strptime(sailing["departureDate"], "%Y-%m-%d").date()
        for room in sailing["categoryPrices"]:
            if room["name"] in "interior":
                interior_price = room["price"] if room["price"] is not None else "N/A"
            elif room["name"] in "outside":
                outside_price = room["price"] if room["price"] is not None else "N/A"
            elif room["name"] in "balcony":
                balcony_price = room["price"] if room["price"] is not None else "N/A"
            elif room["name"] in "concierge":
                suite_price = room["price"] if room["price"] is not None else "N/A"
        parsed_results.append(
            [destination_code, destination_code, ship_code, ship_name, itinerary_id, "Celebrity Cruises", itinerary_id,
             title,
             duration, sail_date, arrive_date, interior_price, interior_price, balcony_price,
             suite_price, ports])
write_file_to_excell(parsed_results)